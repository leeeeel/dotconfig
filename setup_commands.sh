# Install packages
cat pacman.lst | xargs pacman -S needed -noconfirm

# Change xorg keymap
localectl set-x11-keymap br

# Create link between zshenv files
ln -s ~/.config/zsh/.zshenv ~/.zshenv

# Unmute alsa master channel
amixer sset Master unmute

# Enable syncthing service
systemctl enable --now --user syncthing.service

# Remove ugly colorscipts
colorscript -b xmonad 
colorscript -b tux
colorscript -b thebat
colorscript -b tiefighter2
colorscript -b tiefighter-no-invo
colorscript -b tiefighter1-no-invo
colorscript -b suckless
colorscript -b rupees
colorscript -b pukeskull
colorscript -b spectrum
colorscript -b print256
colorscript -b pinguco
colorscript -b kaisen
colorscript -b illumina
colorscript -b hex
colorscript -b elfman
colorscript -b doom-original
colorscript -b doom-outlined
colorscript -b colorwheel
colorscript -b colorview
colorscript -b colortest-slim
colorscript -b colortest
colorscript -b blacklisted
colorscript -b awk-rgb-gradient 
colorscript -b awk-rgb-test
colorscript -b 00default.sh

